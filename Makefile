export AML_LIBS_BUILD_DIR=$(shell pwd)
export AML_LIBS_STAGING_DIR=$(STAGING_DIR)
export AML_LIBS_TARGET_DIR=$(TARGET_DIR)
export AML_LIBS_CFLAGS=$(CFLAGS) -fPIC -g

all:
	mkdir -p $(AML_LIBS_STAGING_DIR)/usr/lib/aml_libs
	mkdir -p $(AML_LIBS_STAGING_DIR)/usr/lib/libplayer
	-$(MAKE) -C amavutils all
	-$(MAKE) -C amcodec all

install:
	-$(MAKE) -C amavutils install
	-$(MAKE) -C amcodec install
	cp -rf $(AML_LIBS_STAGING_DIR)/usr/lib/aml_libs/*.so $(AML_LIBS_TARGET_DIR)/usr/lib
	cp -rf $(AML_LIBS_STAGING_DIR)/usr/lib/aml_libs/*.so $(AML_LIBS_STAGING_DIR)/usr/lib/libplayer

clean:
	-$(MAKE) -C amavutils clean
	-$(MAKE) -C amcodec clean
